import $ from "jquery";
import Prism from 'prismjs';

window.$ = $;


$(function () {

// menu and search

	$('.header__top').on('click', 'button', function (e) {

		var toggle = '#' + $(this).attr('id'),
			menu = '.' + $(this).attr('data-menu');

		if ($(menu).hasClass('is-active')) {
			$('#toggle-menu, #toggle-search, .mainmenu, .search').removeClass('is-active');
			$('#toggle-menu, #toggle-search').attr('aria-expanded', 'false');

			this.setAttribute('aria-expanded', 'false');
			$(menu).removeClass('is-active');
			$(toggle).removeClass('is-active');
		} else {
			$('#toggle-menu, #toggle-search, .mainmenu, .search').removeClass('is-active');
			$('#toggle-menu, #toggle-search').attr('aria-expanded', 'false');

			$(toggle).addClass('is-active');
			$(menu).addClass('is-active');
			this.setAttribute('aria-expanded', 'true');
			$('#search').trigger('focus');
		}

	});

// Prism
	Prism.highlightAll();

// HTML example code 
	$('.html__title').on('click', function (e) {
		
		if( $(this).hasClass('is-active') ) {
			$(this).attr("aria-expanded","false");
			$(this).removeClass("is-active");
			$(this).next('.html__code').removeClass('is-active');
		} else {
			$(this).attr("aria-expanded","true");
			$(this).addClass("is-active");
			$(this).next('.html__code').addClass('is-active');
		}
	});

// Alerts
	var alertDismiss = [];

	$('.close').on('click', function() {
		var alerts 	= $(this).attr('data-alert');

		if(sessionStorage.alertDismiss) {
			alertDismiss= JSON.parse(sessionStorage.getItem('alertDismiss'));
		} 

		alertDismiss.push(alerts);
		sessionStorage.setItem('alertDismiss', JSON.stringify(alertDismiss));

		$('div[data-alert="'+ alerts+'"]').remove();

	});

	var retrievedObject = sessionStorage.getItem('alertDismiss');

	if (retrievedObject !== null) {
		JSON.parse(retrievedObject).forEach(
			element => $('div[data-alert="'+ element+'"]').remove()
		);
	}

// Checkboxes
$('.label__checkbox').on('click', function() {
	$(this).toggleClass('active');
});

$('.hello').on('click', function() {
	$(this).toggleClass('world');
});


	// $('.label__checkbox').on('click', function(e) {
	// 	e.stopPropagation();

	// 	if($(this).hasClass('active')) {
	// 		$(this).removeClass('active');
	// 	} else {
	// 		$(this).addClass('active');
	// 	}
	// });

});