<?php

/**
 * @file
 * Contains hook implementation to add MIME-based attachment to email.
 */

use Drupal\Component\Transliteration\PhpTransliteration;
use Drupal\Core\File\MimeType\ExtensionMimeTypeGuesser;

/**
 * Implements hook_mail_alter().
 *
 * Check if attachment was specified in message params.
 *
 * @param array $message
 */
function email_attachment_mail_alter(array &$message) {
  if (!empty($message['params']['attachment']) || !empty($message['params']['attachments'])) {
    _convert_to_multipart($message);
  }
}

/**
 * Do the actual work.
 *
 * @param array $message
 *   From hook.
 */
function _convert_to_multipart(array &$message) {
  $params = $message['params'];
  $uid = "qd-" . md5("Drupal" . uniqid(time(), TRUE));

  $aHeaders = &$message['headers'];
  $sOrigContentType = $aHeaders['Content-Type'];
  $aHeaders['Content-Type'] = "multipart/mixed; boundary=\"$uid\"";

  $lines = "This is a multi-part message in MIME format.\r\n";
  $lines .= "--$uid\r\n";
  $lines .= "Content-Type: $sOrigContentType \r\n\r\n";
  $lines .= wordwrap(implode('', $message['body'])) . "\r\n\r\n";

  if (!empty($params['attachments'])) {
    if (!is_array($params['attachments'])) {
      throw new InvalidArgumentException("[attachments] doesn't have the right structure.");
    }
    foreach ($params['attachments'] as $aAttachment) {
      $lines .= _add_attachment($uid, $aAttachment);
    }
  }
  if (!empty($params['attachment'])) {
    $lines .= _add_attachment($uid, $params['attachment']);
  }
  $lines .= "--$uid--";

  $message['body'] = [$lines];
}

/**
 * @param string $uid the MIME separator
 * @param array $aAttachment a structure on what to attach
 *
 * @return string
 */
function _add_attachment($uid, array $aAttachment): string {
  if (empty($aAttachment['filename'])) {
    throw new InvalidArgumentException("Attachment doesn't have the right structure.");
  }

  if (empty($aAttachment['filecontent'])) {
    if (!is_file($aAttachment['filename'])) {
      throw new \Drupal\Core\File\Exception\FileNotExistsException("Attachment doesn't have the right structure.");
    }
    $aAttachment['filecontent'] = file_get_contents($aAttachment['filename']);
  }

  $sFileContent = $aAttachment['filecontent'];
  $sChunkedAttachment = chunk_split(base64_encode($sFileContent));

  $transliteration = new PhpTransliteration;
  $filename = $transliteration->transliterate($aAttachment['filename']);
  $sMailAttachmentFriendlyName = basename($filename);

  if (empty($aAttachment['filemime'])) {
    $guesser = new ExtensionMimeTypeGuesser(Drupal::moduleHandler());
    $aAttachment['filemime'] = $guesser->guess($filename);
  }

  $lines = "--$uid\r\n";
  $lines .= "Content-Type: $aAttachment[filemime];\r\n" .
    _encode_header_rfc_2184('name', $sMailAttachmentFriendlyName);
  $lines .= "Content-Transfer-Encoding: base64\r\n";
  $lines .= "Content-Disposition: attachment;\r\n" .
    _encode_header_rfc_2184('filename', $sMailAttachmentFriendlyName);
  $lines .= "\r\n" . $sChunkedAttachment . "\r\n\r\n";

  return $lines;
}

function _encode_header_rfc_2184($name, $value) {
  $lines = str_split($value, 64 - strlen($name));
  foreach ($lines as $i => $line) {
    $lines[$i] = "  $name*$i=\"" . $line . "\"";
  }
  return join(";\r\n", $lines) . "\r\n";
}

/**
 * Implements hook_module_implements_alter().
 *
 * @param $implementations
 * @param $hook
 */
function email_attachment_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'mail_alter') {
    // Let our hook run last so that other modules can add attachments using
    // the 'params' key. See the file 'email_attachment_demo.module' for an
    // example.
    $hookInit = $implementations['email_attachment'];
    unset($implementations['email_attachment']);
    $implementations['email_attachment'] = $hookInit;
  }
}
